const mongoose = require('mongoose');

/* mongoose.connect(process.env.MONGODB_URI,{
    useNewUrlParser:true
})
.then(db=>console.log('DB conectada'))
.catch(err=>console.error(err)); */

//mongodb+srv://desarrollo:<password>@cluster0.6wt76.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
const uri = `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@cluster0.6wt76.mongodb.net/${process.env.DBNAME}?retryWrites=true&w=majority`;
mongoose.connect(uri,
    { useNewUrlParser: true, useUnifiedTopology: true }
)
.then(() => console.log('Base de datos conectada'))
.catch(e => console.log('error db:', e))