const { Router } = require('express');
const router = Router();

const User = require('../models/user.model');
const bcrypt = require('bcryptjs');


router.get('/', async (req, res) => {
    const users = await User.find();
    res.json(users);
});

router.post('/', async (req, res) => {

    const isEmailExist = await User.findOne({ email: req.body.email });
    if (isEmailExist) {
        return res.status(400).json({ error: 'Email ya registrado' })
    }

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);

  /*   const { name, email, permissionLevel, role } = req.body;
    const newUser = new User({ name, email, password, permissionLevel, role }); */

    const newUser = new User({
        name: req.body.name,
        permissionLevel: req.body.permissionLevel,
        role: req.body.role,
        email: req.body.email,
        password: password
    });

    try {
        const savedUser = await newUser.save();
        res.json({
            error: null,
            data: savedUser
        })
    } catch (error) {
        res.status(400).json({ error })
    }
});

router.get('/:id', async (req, res) => {
    await User.findByIdAndDelete(req.params.id);
    res.send({ message: 'Usuario eliminado' });
});

router.get('detail/:id', async (req, res) => {  
    const users = await User.findById(req.params.id);
    res.json(users);
});

module.exports = router;