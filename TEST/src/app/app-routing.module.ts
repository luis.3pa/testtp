import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuardService } from 'src/app/authentication/_guards/guard.service';

const routes: Routes = [
  { path: 'user', loadChildren: () => import('./components/users/users.module').then(m => m.UsersModule),canActivate: [GuardService],
  data: {
    expectedPermission:[1]
  } },
  { path: 'nebular', loadChildren: () => import('./components/admin-layout/admin-layout.module').then(m => m.AdminLayoutModule),canActivate: [GuardService],
  data: {
    expectedPermission: [2]
  } },
  { path: 'auth', loadChildren: () => import('./components/auth/auth.module').then(m => m.AuthModule)},
  { path: '**', redirectTo: 'auth' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
