import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Gallery } from '../models/gallery';


const cabecera = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})

export class GalleryService {
  constructor(private httpClient: HttpClient) { }

  public get(): Observable<Gallery[]> {
    return this.httpClient.get<Gallery[]>(`${environment.api}/externa`, cabecera);
  }
}
