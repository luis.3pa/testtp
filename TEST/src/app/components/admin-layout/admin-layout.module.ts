import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminLayoutComponent } from './admin-layout.component';
import { AdminLayoutRoutingModule } from './admin-layout-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { StatePipe } from '../../util//state.pipe';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbThemeModule, NbLayoutModule, NbIconModule, NbSidebarModule, NbMenuModule, NbCardModule, NbButtonModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';



@NgModule({
  declarations: [AdminLayoutComponent,HomeComponent, StatePipe],
  imports: [
    CommonModule,
    AdminLayoutRoutingModule,
    Ng2SmartTableModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbIconModule,              
    NbSidebarModule.forRoot(),  
    NbMenuModule.forRoot(), 
    NbCardModule,
    NbButtonModule
  ]
})
export class AdminLayoutModule { }
