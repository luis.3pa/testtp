import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/authentication/_auth/token.service';
import { Gallery } from 'src/app/models/gallery';
import { GalleryService } from 'src/app/services/gallery.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  name:string;
  permission:string;
  query:string;
  gallerys:Gallery[];

  settings = {};

  constructor(private tokenService: TokenService,
    private galleryService:GalleryService) { 
    this.name=this.tokenService.getUserName();
    this.permission=this.tokenService.getPermission();
    this.query="";


    this.settings = {
      actions: {
        columnTitle: '',
        add: false,
        edit: false,
        delete: false,
        position: 'right'
      },
      columns: {
        id: {
          title: 'ID'
        },
        title: {
          title: 'TITULO'
        },
        url: {
          filter: false,
          title: 'IMAGEN',
          type: 'html',
          valuePrepareFunction: data => `<img src="` + data + `" class="w-100 shadow-1-strong rounded mb-4">`,
        },
       
      }
    };
  }

  ngOnInit(): void {
    this.galleryService.get().subscribe(data=>{
      this.gallerys=data;
    });
  }

  changePipe(){

    if(this.query==""){
      this.query="ingles";
    }else{
      this.query="";
    }
  }

}
