import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @Input() nameDetail:string;
  @Output() nameReturn = new EventEmitter<string>();
    constructor() {
      this.nameDetail="";
      //this.nameReturn="";
     }

  ngOnInit(): void {
  }

  send(){
    this.nameReturn.emit("");
  }

}
