import { Component, OnInit } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
/* import { NbToastrService } from '@nebular/theme'; */
import { TransferService } from 'src/app/services/transfer.service';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {

  name:string;
  constructor(private transferService:TransferService,
    private toastrService: NbToastrService) {
    this.name="";
   }

  ngOnInit(): void {
  }

  send(){
    this.transferService.name=this.name;
     this.toastrService.show(
      status || 'Success',
      `Cambio titulo`,
      );
  }

}
